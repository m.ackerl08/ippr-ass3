import numpy as np
import imageio
from scipy.ndimage.filters import gaussian_filter
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import math_tools

def lambda_mat_func(eig_val, alpha, gamma):
    return np.array([[alpha, 0.0], 
                    [
                     0.0,
                     alpha + (1.0-alpha)*(1.0-np.exp(-np.abs(eig_val[0] - eig_val[1])**2 / (2.0 * gamma**2)))
                    ]])

# def S_element_func(uxx, uxy, uyy):
#     return np.array([[uxx[0], uxy],
#                      [uxy, uyy]])

lambda_mat_vectorized = np.vectorize(lambda_mat_func)
 
def diffusion_tensor(im, nabla, sigma_g, sigma_u, alpha, gamma):
    """
    Implement the diffusion tensor computation (Eq. 9)
    sigma: u=1, g=2
    """
    size_x = im.shape[0]
    size_y = im.shape[1]
    print("im", im.shape)
    print("nabla", nabla.shape)

    im = gaussian_filter(im, sigma_u)

    #calculate the gradients in x, y direction
    grad_flattened = nabla @ im.flatten()
    grad = grad_flattened.reshape((2, size_x, size_y))
    grad_x = grad[0]
    grad_y = grad[1]
 
    # grad_x = np.gradient(im, axis=0)
    # grad_y = np.gradient(im, axis=1)

    #grad_x = grad_flattened[:size_x*size_y].reshape([size_x, size_y])
    #grad_y = grad_flattened[size_x*size_y:].reshape([size_x, size_y])

    # retrieve directional derivatives and applay first gauss kernel (sigma1=sigma_u)
    #grad_x = gaussian_filter(grad_x, sigma_u)
    #grad_y = gaussian_filter(grad_y, sigma_u)
    #print("grad_flattened", grad_flattened.shape)
    #print("grad", grad.shape, grad_x.shape, grad_y.shape)

    # compute the 4 structure tensor parts for all pixels respectively
    S_xx = gaussian_filter(np.power(grad_x, 2.0), sigma_g)
    S_yy = gaussian_filter(np.power(grad_y, 2.0), sigma_g)
    S_xy = gaussian_filter(grad_x * grad_y, sigma_g)
    print("S_sub", S_xx.shape, S_yy.shape, S_xy.shape)

    # S = np.array([[S_xx.flatten(), S_xy.flatten()], 
    #               [S_xy.flatten(), S_yy.flatten()]])
    # print("S", S.reshape(19800, 2, 2).shape)
    # exit()
    
    #compute D for each pixel in the image D i shape MNx2x2
    S = np.zeros((size_x*size_y, 2, 2))
    for pix_y in range(size_y):
        for pix_x in range(size_x):
            S[(pix_x * size_y) + pix_y] = np.array([[S_xx[pix_x, pix_y], S_xy[pix_x, pix_y]], 
                                                    [S_xy[pix_x, pix_y], S_yy[pix_x, pix_y]]])

    #S = (np.array([[S_xx.flatten(),S_xy.flatten()],[S_xy.flatten(),S_yy.flatten()]])).T

    # S_xx_flat = S_xx.flatten().reshape((size_x * size_y, 1)) # for apply_along_axis the "main" array needs to be shape [x,1] instead of shape [x]q 
    # S_xy_flat = S_xy.flatten()
    # S_yy_flat = S_yy.flatten()

    # S = np.apply_along_axis(S_element_func, 1, S_xx_flat, S_xy_flat, S_yy_flat)
    # print("S", S.shape)
    # print(S)

    # .eigh can be used for symmetric matrizes. the resulting values are sorted after eigenvalues in ascending order -> flip for descending order
    eig_val, eig_vec = np.linalg.eigh(S)
    eig_val = np.flip(eig_val, 1)
    # doc says the resulting eigenvectors are column vectors
    eig_vec = np.flip(eig_vec, 2)
        
    print("eig", eig_val.shape, eig_vec.shape) 

    # D = np.zeros((size_x*size_y, 2, 2))
        
    lambda_mat = np.apply_along_axis(lambda_mat_func, 1, eig_val, alpha, gamma)
    print("lambda_mat", lambda_mat.shape)

    #D = eig_vec @ lambda_mat @ eig_vec 

    D = eig_vec @ lambda_mat @ np.transpose(eig_vec, axes=(0,2,1))  # eig_vec seems to be diagonal matritzes -> no need to transpose 
    # print("D", D.shape)

    #compute the final D(U_t) by computing the diagonals matrixes from each of the 2x2 parts of D
    D_bold = sp.vstack([sp.hstack([sp.diags(D[:,0,0]), sp.diags(D[:,0,1])]),
                        sp.hstack([sp.diags(D[:,0,1]), sp.diags(D[:,1,1])])])

    return D_bold

def stopping_edge(x, gamma):
    return np.exp(- x**2 / (2 * gamma**2))

def coherence_enhancing_diffusion(
    image, sigma_g, sigma_u,
    alpha, gamma, tau, end_time
):
    time = 0
    # image = np.array([[0,0,0], [0,1,1], [0,1,0]], dtype=np.float64)
    # print(image)
    U_t = image.flatten()

    nabla = math_tools.spnabla_hp(image.shape[0], image.shape[1])
    while time < end_time:
        time += tau
        print(f'{time}')
        # updated the diffusor_tensor() to take nabla as argument
        D = diffusion_tensor(
            U_t.reshape(image.shape), nabla, sigma_g, sigma_u, alpha, gamma
        )
        print("D", D.shape)
        mat = sp.eye(U_t.shape[0], format='csr') + tau * nabla.T @ D @ nabla
        U_t = spsolve(mat, U_t)
        #print(U_t)
    return np.reshape(U_t, image.shape)


if __name__ == "__main__":
    sigma_u = 0.7 # sigma 1
    sigma_g = 1.5 # sigma 2
    alpha = 0.0005
    gamma = 0.0001
    tau = 5
    endtime = 100
    input = imageio.imread("input.jpg").mean(axis=2)/255.0
    output = coherence_enhancing_diffusion(
        input, sigma_g, sigma_u, alpha, gamma, tau, endtime
        )
    imageio.imsave('./output.jpg', output)
    #input = imageio.imread("input_handy.jpg").mean(axis=2)/255.0
    #for j in range(4):
    #    output = coherence_enhancing_diffusion(
    #    input, sigma_g, sigma_u, alpha, gamma, tau, endtime
    #    )
    #    imageio.imsave('./output_alpha_'+str(alpha)+'_gamma_'+str(gamma)+'.jpg', output)
    #    alpha *= 10  

